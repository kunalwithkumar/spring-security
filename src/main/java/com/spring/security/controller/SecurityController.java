package com.spring.security.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.spring.security.entity.User;
import com.spring.security.service.SecurityServiceImpl;

@RestController
@RequestMapping("/api/v1/security")
@CrossOrigin
public class SecurityController {

	@Autowired
	private SecurityServiceImpl securityService;

	@GetMapping("/hello")
	public ResponseEntity<String> sayHello() {
		return new ResponseEntity<>("Hello from endpoint", HttpStatus.OK);
	}

	@GetMapping("/all")
	@Secured("ADMIN")
	public ResponseEntity<List<User>> getAllUsers() {

		return securityService.getAllUsers();
	}

	@GetMapping("/user/{email}")
	@Secured("USER")
	public ResponseEntity<User> getUserByMail(@PathVariable final String email) {

		return securityService.getUserByMail(email);
	}

}
