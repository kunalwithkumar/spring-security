package com.spring.security.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.spring.security.config.JwtService;
import com.spring.security.entity.AuthenticationRequest;
import com.spring.security.entity.AuthenticationResponse;
import com.spring.security.entity.RegisterRequest;
import com.spring.security.entity.Role;
import com.spring.security.entity.Token;
import com.spring.security.entity.TokenType;
import com.spring.security.entity.User;
import com.spring.security.repository.TokenRepo;
import com.spring.security.repository.UserRepository;

@Service
public class AuthenticationService {

	@Autowired
	private UserRepository repository;

	@Autowired
	private TokenRepo tokenRepository;
	@Autowired
	private PasswordEncoder passwordEncoder;
	@Autowired
	private JwtService jwtService;

	@Autowired
	private AuthenticationManager authenticationManager;

	public AuthenticationResponse register(final RegisterRequest request) {

		final User user = new User();

		user.setEmail(request.getEmail());
		user.setFirstName(request.getFirstname());
		user.setLastName(request.getLastname());
		user.setPassword(passwordEncoder.encode(request.getPassword()));
		user.setRole((Role.USER));

		final User savedUser = repository.save(user);
		final String jwtToken = jwtService.generateToken(user);
		saveUserToken(savedUser, jwtToken);

		// creating admin user
		final User user2 = new User();

		user2.setEmail("admin@mail.com");
		user2.setFirstName("Admin");
		user2.setLastName("User");
		user2.setPassword(passwordEncoder.encode("123"));
		user2.setRole((Role.ADMIN));

		final User savedUser2 = repository.save(user2);
		final String jwtToken2 = jwtService.generateToken(user2);
		saveUserToken(savedUser2, jwtToken2);

		return new AuthenticationResponse(jwtToken, savedUser2);
	}

	public AuthenticationResponse authenticate(final AuthenticationRequest request) {
		authenticationManager
				.authenticate(new UsernamePasswordAuthenticationToken(request.getEmail(), request.getPassword()));
		final User user = repository.findByEmail(request.getEmail())
				.orElseThrow(() -> new UsernameNotFoundException("User Not Found"));
		final String jwtToken = jwtService.generateToken(user);
		revokeAllUserTokens(user);
		saveUserToken(user, jwtToken);
		return new AuthenticationResponse(jwtToken, user);
	}

	private void saveUserToken(final User user, final String jwtToken) {
		final Token token = new Token();

		token.setUser(user);
		token.setToken(jwtToken);
		token.setTokenType(TokenType.BEARER);
		token.setExpired(false);
		token.setRevoked(false);

		tokenRepository.save(token);
	}

	private void revokeAllUserTokens(final User user) {
		final List<Token> validUserTokens = tokenRepository.findAllValidTokenByUser(user.getId());
		if (validUserTokens.isEmpty())
			return;
		validUserTokens.forEach(token -> {
			token.setExpired(true);
			token.setRevoked(true);
		});
		tokenRepository.saveAll(validUserTokens);
	}

}
