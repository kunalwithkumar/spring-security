package com.spring.security.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.spring.security.entity.User;
import com.spring.security.repository.UserRepository;

@Service
public class SecurityServiceImpl {

	@Autowired
	private UserRepository userRepository;

	public ResponseEntity<List<User>> getAllUsers() {

		return new ResponseEntity<>(userRepository.findAll(), HttpStatus.OK);
	}

	public ResponseEntity<User> getUserByMail(final String email) {

		final User user = userRepository.findByEmail(email).get();
		return new ResponseEntity<>(user, HttpStatus.OK);
	}
}
