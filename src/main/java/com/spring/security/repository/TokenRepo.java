package com.spring.security.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.spring.security.entity.Token;

@Repository
public interface TokenRepo extends JpaRepository<Token, Integer> {

	@Query("from Token where user.id = ?1 and revoked= false")
	List<Token> findAllValidTokenByUser(Integer id);

	Optional<Token> findByToken(String token);
}
