package com.spring.security.entity;

public class AuthenticationRequest {

	private String email;
	private String password;

	public String getEmail() {
		return email;
	}

	public void setEmail(final String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(final String password) {
		this.password = password;
	}

	public AuthenticationRequest(final String email, final String password) {
		this.email = email;
		this.password = password;
	}

	public AuthenticationRequest() {
		// TODO Auto-generated constructor stub
	}

}
