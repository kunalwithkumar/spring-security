package com.spring.security.entity;

public class AuthenticationResponse {

	private String token;

	private User user;

	public String getToken() {
		return token;
	}

	public void setToken(final String token) {
		this.token = token;
	}

	public AuthenticationResponse() {
	}

	public AuthenticationResponse(final String token) {
		this.token = token;
	}

	public User getUser() {
		return user;
	}

	public void setUser(final User user) {
		this.user = user;
	}

	public AuthenticationResponse(final String token, final User user) {
		this.token = token;
		this.user = user;
	}

}
